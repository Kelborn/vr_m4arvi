// Fill out your copyright notice in the Description page of Project Settings.

#include "AutoGameSession.h"

#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Online.h"

void AAutoGameSession::RegisterServer()
{
	IOnlineSubsystem* const onlineSub = IOnlineSubsystem::Get();
	if (!onlineSub)
	{
		return;
	}
	IOnlineSessionPtr sessionInt = onlineSub->GetSessionInterface();
	if (!sessionInt)
	{
		return;
	}

	FOnlineSessionSettings Settings;
	Settings.NumPublicConnections = 4;
	Settings.bShouldAdvertise = true;
	Settings.bAllowJoinInProgress = true;
	Settings.bIsLANMatch = true;
	Settings.bUsesPresence = true;
	Settings.bAllowJoinViaPresence = true;

	sessionInt->CreateSession(0, GameSessionName, Settings);
	return;
}