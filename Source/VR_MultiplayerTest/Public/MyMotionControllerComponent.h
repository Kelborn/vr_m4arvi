// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MotionControllerComponent.h"
#include "MyMotionControllerComponent.generated.h"

/**
 * 
 */
UCLASS()
class VR_MULTIPLAYERTEST_API UMyMotionControllerComponent : public UMotionControllerComponent
{
	GENERATED_BODY()
	
};
