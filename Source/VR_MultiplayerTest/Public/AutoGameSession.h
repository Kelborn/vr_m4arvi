// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameSession.h"
#include "AutoGameSession.generated.h"

/**
 * 
 */
UCLASS()
class VR_MULTIPLAYERTEST_API AAutoGameSession : public AGameSession
{
	GENERATED_BODY()

		virtual void RegisterServer() override;
};
